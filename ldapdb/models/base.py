# -*- coding: utf-8 -*-
# 
# django-ldapdb
# Copyright (c) 2009-2011, Bolloré telecom
# All rights reserved.
# 
# See AUTHORS file for a full list of contributors.
# 
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
# 
#     1. Redistributions of source code must retain the above copyright notice, 
#        this list of conditions and the following disclaimer.
#     
#     2. Redistributions in binary form must reproduce the above copyright 
#        notice, this list of conditions and the following disclaimer in the
#        documentation and/or other materials provided with the distribution.
# 
#     3. Neither the name of Bolloré telecom nor the names of its contributors
#        may be used to endorse or promote products derived from this software
#        without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import ldap
import logging

import django.db.models
from django.db import connections, router
from django.db.models import signals

import ldapdb
import fields

# Dict containing 
object_classes = {}

class Model(django.db.models.base.Model):
    """
    Base class for all LDAP models.
    """
    dn = django.db.models.fields.CharField(max_length=200)

    # meta-data
    base_dn = None
    search_scope = ldap.SCOPE_SUBTREE
    object_classes = ['top']

    def __init__(self, *args, **kwargs):
        super(Model, self).__init__(*args, **kwargs)
        self.saved_pk = self.pk

    def build_rdn(self):
        """
        Build the Relative Distinguished Name for this entry.
        """
        bits = []
        for field in self._meta.fields:
            if field.db_column and field.primary_key:
                bits.append("%s=%s" % (field.db_column, getattr(self, field.name)))
        if not len(bits):
            raise Exception("Could not build Distinguished Name")
        return '+'.join(bits)

    def build_dn(self):
        """
        Build the Distinguished Name for this entry.
        """
        return "%s,%s" % (self.build_rdn(), self.base_dn)
        raise Exception("Could not build Distinguished Name")

    def delete(self, using=None):
        """
        Delete this entry.
        """
        using = using or router.db_for_write(self.__class__, instance=self)
        connection = connections[using]
        logging.debug("Deleting LDAP entry %s" % self.dn)
        connection.delete_s(self.dn)
        signals.post_delete.send(sender=self.__class__, instance=self)

    def save(self, using=None):
        """
        Saves the current instance.
        """
        using = using or router.db_for_write(self.__class__, instance=self)
        connection = connections[using]
        if not self.dn:
            # create a new entry
            record_exists = False 
            entry = [('objectClass', self.object_classes)]
            new_dn = self.build_dn()

            for field in self._meta.fields:
                if not field.db_column:
                    continue
                value = getattr(self, field.name)
                if value:
                    entry.append((field.db_column, field.get_db_prep_save(value, connection=connection)))

            logging.debug("Creating new LDAP entry %s" % new_dn)
            connection.add_s(new_dn, entry)

            # update object
            self.dn = new_dn

        else:
            # update an existing entry
            record_exists = True
            modlist = []
            modlist.append((ldap.MOD_REPLACE, 'objectClass', [x.encode(connection.charset) for x in self.object_classes]))
            orig = self.__class__.objects.get(pk=self.saved_pk)
            for field in self._meta.fields:
                if not field.db_column:
                    continue
                old_value = getattr(orig, field.name, None)
                new_value = getattr(self, field.name, None)
                if old_value != new_value:
                    if new_value:
                        modlist.append((ldap.MOD_REPLACE, field.db_column, field.get_db_prep_save(new_value, connection=connection)))
                    elif old_value:
                        modlist.append((ldap.MOD_DELETE, field.db_column, None))

            if len(modlist):
                # handle renaming
                new_dn = self.build_dn()
                if new_dn != self.dn:
                    logging.debug("Renaming LDAP entry %s to %s" % (self.dn, new_dn))
                    connection.rename_s(self.dn, self.build_rdn())
                    self.dn = new_dn
            
                logging.debug("Modifying existing LDAP entry %s" % self.dn)
                connection.modify_s(self.dn, modlist)
            else:
                logging.debug("No changes to be saved to LDAP entry %s" % self.dn)

        # done
        self.saved_pk = self.pk
        signals.post_save.send(sender=self.__class__, instance=self, created=(not record_exists))

    def add_object_class(self, oc):
        """
        Add an extra object class to this object. The added objectclass
        must be defined as a subclass of ObjectClass. This changes the
        type of this object to add the fields of the new
        objectclass.

        The objectclass passed can be a string naming the objectclass,
        or an ObjectClass subclass.
        """

        # The new class is a subclass of Model
        bases = [Model] + list(Model.__bases__)

        object_classes = self.__class__.object_classes
        object_classes.append(oc)

        dict_ = {
            # Copy the module from Model
            '__module__': Model.__module__,
            # Generate some documentation
            '__doc__': 'Automatically generated class for LDAP objects with objectclasses: ' + (', '.join(object_classes)),
            # Add a class variable containing the object_classes
            'object_classes': object_classes,
            # Copy the base_dn from the old class
            'base_dn': self.__class__.base_dn,
        }

        # Add all current fields, but leave out fields that Model
        # already has (these will be added by Django later, since we
        # make the new class a subclass of Model).
        dict_.update([(f.name, f) for f in self._meta.fields if not f in Model._meta.fields])

        # If the passed object class is a string, resolve it to an
        # ObjectClass object.
        if (isinstance(oc, basestring)):
            oc = ObjectClass.object_classes[oc]

        # Copy all fields (e.g., subclasses of Field) from the new
        # ObjectClass
        for k,v in oc.__dict__.items():
            if isinstance(v, django.db.models.fields.Field):
                dict_[k] = v

        # Generate a name for the class (gotta have something...)
        name = '_'.join(object_classes)

        # And finally, generate the type
        self.__class__ = type(name, tuple(bases), dict_)

    # TODO: remove_object_class

    @classmethod
    def scoped(base_class, base_dn):
        """
        Returns a copy of the current class with a different base_dn.
        """
        import new
        import re
        suffix = re.sub('[=,]', '_', base_dn)
        name = "%s_%s" % (base_class.__name__, str(suffix))
        new_class = new.classobj(name, (base_class,), {'base_dn': base_dn, '__module__': base_class.__module__})
        return new_class

    class Meta:
        abstract = True

class ObjectClass(object):
    """
    Superclass for LDAP objectclass descriptors.
    """

    # Mapping from objectclass name (string) to the ObjectClass subclass
    # that describes it. Used when loading items from the database or
    # when adding object classes.
    #
    # TODO: Automatically fill this list from a fancy metaclass
    object_classes = {}

